import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Service } from '../.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  images: any;
  response: any;

  constructor(private router: Router, private service : Service) { }

  ngOnInit() {
    this.showNames();
  }

  showNames(){
    debugger
  this.service.showList().subscribe(res => {
    this.response = res;
    debugger
  })
}
  details(value){
    // this.service.showImages(value).subscribe(res =>{
    //   this.images= res.imageUrl;
      this.router.navigate(['/images', value]);
      debugger
    // })
    // this.router.navigateByUrl('/images');
  }
}
