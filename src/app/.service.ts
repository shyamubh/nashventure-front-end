import { Injectable } from '@angular/core';
import { Headers, Http, Jsonp, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class Service {

  constructor(private http: Http) { }


  saveImage(search)
  {
    let url = "http://localhost:3000/saveimage";
    let header : Headers = new Headers();
    header.append("Content_type", "application/json");
    let options = new RequestOptions({headers : header})
    let body = {
      searchText : search,
    }
  return this.http.post(url, body, options).map(res => res.json())
  }


  showList(){
    let url = "http://localhost:3000/imagelist";
    let header : Headers = new Headers();
    header.append("Content_type", "application/json");
    let options = new RequestOptions({headers : header})
    debugger
  return this.http.get(url, options).map(res => res.json())
  }

  showImages(value){
    let url = "http://localhost:3000/images";
    let header : Headers = new Headers();
    header.append("Content_type", "application/json");
    let options = new RequestOptions({headers : header})
    debugger
  return this.http.get(url+ "?searchText=" + value, options).map(res => res.json())
  }
}


