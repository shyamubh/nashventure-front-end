import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SaveimagesComponent } from './saveimages/saveimages.component';
import { ListComponent } from './list/list.component';
import { ImagesComponent } from './images/images.component';
import { AppRouter } from "./app.routing";
import { FormsModule }   from '@angular/forms';
import { Service } from './.service'
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    SaveimagesComponent,
    ListComponent,
    ImagesComponent
  ],
  imports: [
    BrowserModule, AppRouter, FormsModule, HttpModule
  ],
  providers: [Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
