import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveimagesComponent } from './saveimages.component';

describe('SaveimagesComponent', () => {
  let component: SaveimagesComponent;
  let fixture: ComponentFixture<SaveimagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveimagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveimagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
