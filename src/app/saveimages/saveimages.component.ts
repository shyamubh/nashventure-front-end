import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Service } from '../.service';
@Component({
  selector: 'app-saveimages',
  templateUrl: './saveimages.component.html',
  styleUrls: ['./saveimages.component.css']
})
export class SaveimagesComponent implements OnInit {
  response: any;
  search: string;
  
  constructor(private router: Router, private service: Service) { }

  ngOnInit() {
  }

  submit(){
    this.service.saveImage(this.search).subscribe(res => {
      this.response = res;
    alert(this.response);
    })
  }

  list(){
  this.router.navigateByUrl('/list');
  }
}
