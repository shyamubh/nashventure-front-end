import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SaveimagesComponent } from "./saveimages/saveimages.component";
import { ListComponent } from "./list/list.component";
import { ImagesComponent } from "./images/images.component";

const routes: Routes = [
    { path: '' , redirectTo: 'saveimages', pathMatch: 'full'},
    { path: 'saveimages', component: SaveimagesComponent },
    { path: 'list', component: ListComponent },
    { path: 'images/:id', component: ImagesComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRouter{

}