import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Service } from '../.service';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent implements OnInit {
  value: any;
  images: any
  constructor(private route: ActivatedRoute, private service: Service) { }

  ngOnInit() {
    this.route.params.subscribe(res => {
      this.value = res['id'];
      this.details(this.value);
    })
  }

  details(value){
    this.service.showImages(value).subscribe(res =>{
      this.images= res[0].imageUrl;
      debugger
    })
  }  

}
